####################
# Projeto SIM 2017/2018 - Analise de texto
#
# Joao Correia v3
#
#
####################
import io
import numpy as np
import sys
import glob
from stemmer import Stemmer
from sklearn.decomposition import NMF
import string
import re
from collections import Counter

###################

# variaveis globais
st = Stemmer('english')
#  COMPLETAR colocar as variaveis lidas por argumentos
numero_ficheiros_a_analisar = 100
numero_de_topicos = 1

regex = re.compile('[^A-Za-z]')  # so aceita letras, exemplo de aplicao: regex.sub(' ', str)
#

def parseArgs(args):
    # Guardar em variaveis os argumentos pedidos pelo enunciado
    for arg in args:
        print(arg)# neste momento apenas faz print das variaveis passadas por argumento
    pass

# COMPLETAR
# remover da lista texto as palavras contidas no ficheiro stopwords
# parametros esperados :
# texto -  lista com uma palavra por indice da lista
# stop_words - lista com uma stop word por indice da lista
def removeStopWords(texto, stop_words):
    # ADAPTAR  se necessário
    # lista por compreensao que cria uma lista com elementos que nao estejam noutra (neste caso da stopwords)
    texto_limpo = [termo for termo in texto if termo not in stop_words]
    return texto_limpo


def readText(path):
    ##
    texto = []
    # COMPLETAR ler conteudo do ficheiro para uma lista texto, contendo uma palavra por indice da lista
    
    return texto


## devolve lista de ficheiros
def readFiles(path):
    # listar ficheiros de um caminho (path) (assumindo path = /caminho/pasta/)
    file_list = glob.glob(pathname=path + '*')
    print(file_list)
    return file_list


###  COMPLETAR : deve devolver uma lista de documentos ja processado (sem stopwords e na forma reduzida)
def stemWords(text_apos_stopwords):
    text_normalizado = []
    ## utilizar o objeto global st
    # exemplos
    print(st.stemWord('tests'))
    print(st.stemWord('waiting'))
    
    return text_normalizado

def textPreProcessing():
    # COMPLETAR  : passar o caminho para os ficheiros a analisar
    files = readFiles("data_report/")
    docs = []
    terms = []

    # COMPLETAR : carregar o conjunto de stopwords uma vez para poder ser reutilizada ao longo do for
    # ler o ficheiro de stopwords existente (stopwords.txt)
    stop_words = []
    with open("stopwords.txt") as fp:
        lines = fp.readlines()
        for line in lines:
            stop_words.append(line.lower().strip())

    for i in range(numero_ficheiros_a_analisar):
        ## COMPLETAR algo neste sentido

        # 1 remover stopwords do texto
        text_apos_stopwords = removeStopWords(readText(files[i]),stop_words)
        # 2 a partir do texto sem stop words .. criar uma nova lista com as palavras raiz (processo de stemming)
        texto_processado = stemWords(text_apos_stopwords)
        # exit(0)
        docs.append(texto_processado)

    #COMPLETAR criar lista com termos a partir de todos os termos presents em docs (sem repeticoes)
    terms = []
    # ... preencher terms!
    terms.sort()  # ordenar por ordem alfabetica
    
    ## deve devolver docs, terms, 
    return docs, terms, files


# Devolve matriz X com a Term-Frequency - Inverse-Document-Frequency
# sera necessario ter acesso a uma lista de termos e de docs
def termfrequency(docs, terms):  ## completar com argumentos que sejam necessarios
    numeroDePalavras = len(terms)
    numeroDeFicheiros = len(docs)

    # COMPLETAR output dos termos para txt
    with open('terms.csv', 'w') as f:
        for item in terms:
            # output para ficheiro...

    
    X = np.zeros([numeroDePalavras, numeroDeFicheiros])
    ### COMPLETAR calcula termos para uma matriz da package numpy  [numeroDePalavras][numeroDeFicheiros]
    
    # gravar para csv (sugestao)
    np.savetxt("TF.csv", X, delimiter=",")

    
    ### COMPLETAR calcula IDF para uma matriz da package numpy [numeroDePalavras]
    X_idf = np.zeros([numeroDePalavras])
    
    
    ### COMPLETAR calcular tf-idf   para um array numpy [numeroDePalavras][numeroDeFicheiros]
    ##  tfidf[termo][documento] = TF[termo][documento] * IDF[termo]
    X_tfidf = np.zeros([numeroDePalavras, numeroDeFicheiros])
    

    return X_tfidf


# faz o print dos termos e dos pesos
def print_topic_words(W, terms, n_top_words=15):
    n_topics = W.shape[1]
    for t in range(n_topics):
        # argsort devolve lista do indices ordenados
        # como queremos as n_top_words esse indice tem de ser percorrido por ordem inversa (-> [:-n_top_words - 1:-1] )
        sorted_terms_for_topic_idx = W[:, t].argsort()[:-n_top_words - 1:-1]
        list_of_top = []
        list_of_top_w = []
        # por cada indice das topwords .. adicionar a uma lista de termos e pesos
        for idx in sorted_terms_for_topic_idx:
            list_of_top.append(terms[idx])
            list_of_top_w.append(W[idx, t])
        print(t)
        print(list_of_top)
        print(list_of_top_w)




def pipeline():
    ## 1  tratamento do texto:
    # Passos (olhar para dentro da funcao):
    #  (1) ler texto_raw
    #  (2) passar para letra minuscula com a funcao lowercase()
    #  (3) remover pontuacao com ".replace()"
    #  (4) remover stopwords
    #  (5) stemming
    #  (6) construcao da lista de termos e lista de documentos
    list_docs, terms, files = textPreProcessing()


    ## 2  contagem da frequencia dos termos
    # Passos:
    # (1) construcao da matriz TF ( term frequency )
    # (2) construcao da matriz IDF ( inverse document frequency )
    # (3) construcao da matriz TF-IDF
    X_tfidf = termfrequency(list_docs, terms)

    ## 3  aplicacao do NNF  (COMPLETO)
    model = NMF(n_components=numero_de_topicos, init='random', random_state=0)
    W = model.fit_transform(X_tfidf)
    H = model.components_
    ##


    ## 4  Output para consola e ficheiros
    # Passos:
    # (1) analisar resultados olhando para os terms e para W
    # (2) analisar os resultados para os Docs e H
    # (3) fazer output para ficheiro dos resultados. Para cada topico escrever pares :  termo, peso (weight); um por linha
    # exemplo :  topico_1.csv : <term1> , <weight_term1> \n  <term2> , <weight_term2> \n


    # funcao para print das topwords (para analise e debug)
    ## experimentar com n_top_words diferentes para análise se conveniente
    print_topic_words(W, terms, n_top_words=20)


    ## COMPLETAR : output para ficheiro dos topicos e pesos (W) e (H)
    # exportar W e H
    
    pass


if __name__ == '__main__':
    parseArgs(sys.argv)  # COMPLETAR .. fazer o parse (passar para variaveis) dos argumentos pedidos no enunciado
    # funcao principal
    pipeline()
