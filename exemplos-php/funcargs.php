<?php

function ola ()
{ }

function hello (): String {
	return 'Olá ';
}

function adeus (): array {
	return array('Adeus', 'Xau', 1, 2.0);
}

echo ola(); // NULL 
echo hello(); // 'Olá' 
echo var_dump(adeus()); // 'Adeus', 'Xau', 1, 2.0
?>






