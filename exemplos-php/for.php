<?php 
for ($x = 0; $x <= 5; $x++) {
    echo "valor: $x <br>";
} 

echo "<br> ---- <br>";
// for each é frequente para iterar sobre arrays
$items = array("um", "dois", "tres", "quatro", "cinco"); 

foreach ($items as $valor) {
    echo "$valor <br>";
}

?>