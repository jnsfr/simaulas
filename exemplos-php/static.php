<?php
function f()
{
    static $a = 0;
    echo $a . '<br>';
    $a++;
}

f(); // 0
f(); // 1
f(); // 2
f(); // 3

?>