"""
Exemplo de main com import do modulo sys para 
leitura de argumentos pela consola
"""

def metodo():
	print ('ola')

if __name__ == "__main__":
    import sys
    print('Recebi pela consola {0} '.format(sys.argv[1]))
    metodo()