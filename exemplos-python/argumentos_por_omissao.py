#Nota: os argumentos com valor por defeito 
# tem de ser definidos por ultimo
def foo(primeiro , segundo = 1, terceiro = 3):
	print('{0} {1} {2} '.format(primeiro, segundo, terceiro))

foo(10, 2) # sem problema
foo(10, terceiro = 20)
foo(terceiro = 123, primeiro = 111, segundo = 222)
foo(123) # sem problema 
foo() # erro ! falta definir um argumento


