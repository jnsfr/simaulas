# Listas por compreensao podem ter listas contidas (nested)
matriz = [
	[1, 2, 3, 4],
	[5, 6, 7, 8],
	[9, 10, 11, 12]
]
#            ( (      primeira lista        )      segunda     )  
transposta = [ [linha[i] for linha in matriz] for i in range(4)]
print (transposta)
#equivalente
transposta = []
for i in range(4):
# the following 3 lines implement the nested listcomp
	transposta_linha = []
	
	for linha in matriz:
		transposta_linha.append(linha[i])

	transposta.append(transposta_linha)

print (transposta)