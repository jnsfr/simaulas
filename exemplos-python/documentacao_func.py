def funcaoDocumentada(umavariavel, outravariavel= 'teste'):
	"""
	Documentacao da funcaoDocumentada
	argumentos 
	umavariavel - str 
	outravariavel - str

	retorna str
	"""

	return '{0} {1}'.format(umavariavel, outravariavel)

print(funcaoDocumentada.__doc__)
print(funcaoDocumentada('um'))