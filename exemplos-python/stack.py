#uma lista pode funcionar como pilha usando metodos built_in 
x = [1, 2, 3]
print (x)
x.append(4)
print (x)
topo = x.pop()
print (topo)
#podem consultar usando help(var) onde <var> tem de ser uma lista
# help(x)

