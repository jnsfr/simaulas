# valida mas vai ser descontinuada 
# semelhante ao printf teremos de manter a ordem
# de inputs e especificar o tipo 
print 'teste old %s %f' % ('ola', 23.0 ) 

# esta devera ser a forma a adoptar
# {<index>} diz qual a variavel a mostrar
print 'teste format {0} {1}'.format('ola', 23)

# pode ser utilizado para strings
a = 'outro {0}'.format('ola')

print a
