#comentario Listas

lista = [ 'xpto', 123 , 2.233, 'ze', 70.2 ]
outra = [123, 'manel']

print lista          # mostra lista
print lista[0]       # mostra primeiro elemento 
print lista[1:3]     # mostra elementos a partir do segundo ate ao terceiro 
print lista[2:]      # mostra elmentos a partir do terceiro elemento
print outra * 2   # mostra a lista duas vezes
print lista + outra # mostra uma lista com as duas outras listas concatenadas