import package
import package.subpackage.subexemplo

package.exemplo.hello() 
package.subpackage.subexemplo.submetodo()

print('...outra maneira...')
#outra maneira, ir buscar a package o ficheiro 
from package import exemplo
exemplo.hello()
from package.subpackage import subexemplo
subexemplo.submetodo()

print('...ou ainda...')
from package.exemplo import hello
hello()

from package.subpackage.subexemplo import submetodo
submetodo()

