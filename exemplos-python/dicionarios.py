dic = {} # definicao !
dic['um'] = "chave um"
dic[2]     = "chave dois"

exemplo = {'nome': 'john','codigo':1212, 'departamento': 'vendas'}


print dic['um']      # mostra valor para a chave 'um' 
print dic[2]          # mostra valor para a chave 2
print exemplo          # mostra o dicionario completo
print exemplo.keys()   # mostra chaves do dicionario
print exemplo.values() # mostra os valores do dicionario