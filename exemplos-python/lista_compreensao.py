"""
 Listas por compreensao permitem efectuar operacoes sobre os elementos
 de uma dada lista de modo a preenche-la sobre algum criterio.

 Maneira "tradicional" sera fazer ciclos para preencher a lista
"""
#1 - preencher uma lista com os quadrados dos indices
squares = [x**2 for x in range(10)]
print(squares)
# equivalente a 
squares = []
for x in range(10):
	squares.append(x**2)
print(squares)
#2 - podemos colocar condicoes
# quadrados dos elementos maiores que 4 e ate 9
squares = [x**2 for x in range(10) if x > 4]
print (squares)

#3 - complicando ! 
# cria pares de elementos que nao sejam iguais
# entre si 
comb = [(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]
print (comb)
# equivalente a
comb = []
for x in [1,2,3]:
	for y in [3,1,4]:
		if x != y:
			comb.append((x, y))
print(comb)