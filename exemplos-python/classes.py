# sintax para definir uma class, podemos ter varias por ficheiro,
#  identacao e chave para funcionar correctamente
class MyClass:
    """Documentacao vem aqui"""

    def __init__(self):
    	# construtor (Vazio neste caso)
    	# self equivale ao "this" de java/c
    	self.i = 1111111

    # campo publico
    i = 12345

    # metodos
    def f(self):
        return 'hello world'

x = MyClass()
print (x.f())
print (x.i)