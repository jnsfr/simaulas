import numpy as np
import matplotlib.pyplot as plt

# arranjo de pontos 
t = np.arange(0., 5., 0.2)

# t linear -  tracejado vermelho
# t ao quadrado - quadrados azul
# t ao cubo - triangulos verde
plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
plt.show()

