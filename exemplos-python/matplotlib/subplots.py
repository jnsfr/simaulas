import numpy as np
import matplotlib.pyplot as plt

# lista com valores para x com 
# numero de pontos diferentes
x1 = np.linspace(0.0, 5.0)
x2 = np.linspace(0.0, 2.0)

# lista com valores para y com
# tranformacao por cos
y1 = np.cos(2 * np.pi * x1) * np.exp(-x1)
y2 = np.cos(2 * np.pi * x2)

# desenha o subplot 1
plt.subplot(2, 1, 1)
plt.plot(x1, y1, 'o-')
plt.title('Dois subplots')

# desenha subplot 2
plt.subplot(2, 1, 2)
plt.plot(x2, y2, '.-')
plt.xlabel('time (s)')

plt.show()

