import matplotlib.pyplot as plt

# primeira lista tem os valores de x, 
# segunda lista os valores de y
plt.plot([1,2,3,4,5,6,7],[1,4,9,16,25,36,49])
plt.ylabel('Hello poly !')
plt.show()
# limpa figura e eixos para podermos 
# reutilizar a mesma figura 
# de novo chamando "plt"
plt.clf()
plt.cla()

