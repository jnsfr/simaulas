import matplotlib.pyplot as plt
from numpy.random import normal, uniform

numpy.random.seed(0)
gaussian_numbers = normal(size=100)
uniform_numbers = uniform(low=-3, high=3, size=100)
plt.hist(gaussian_numbers, bins=20, normed=True, color='b', alpha =0.5 ,label='Gaussian')
plt.hist(uniform_numbers, bins=20, normed=True, color='g', alpha=0.5, label='Uniform')
plt.title("Gaussian/Uniform Histogram")
plt.xlabel("Value")
plt.ylabel("Probability")
plt.legend()
plt.show()