import matplotlib.pyplot as plt
from numpy.random import normal, uniform

# fixar a seed para poder obter 
# os mesmos resultados sempre que 
# executamos
numpy.random.seed(0)
# criar 100 numeros de uma distruibuicao
# gaussiana (normal)
# por defeito, media 0, desvio padrao 1
gaussian_numbers = normal(size=100)
print gaussian_numbers
print gaussian_numbers.shape
# basta passar o array a funcao plt.hist para 
# criar um histograma de frequencia
plt.hist(gaussian_numbers)
plt.title("Gaussian Histogram")
plt.xlabel("Value")
plt.ylabel("Frequency")
plt.show()

