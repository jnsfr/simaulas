b = 2

def funcao (a):
	a = a * 2
	outro = 10
	print a
	if a > 0 :
		print 'maior que 0'
	# multiplos valores de retorno
	return a , outro

b, c = funcao(b)

print b
print c 

# exemplo de multipla atribuição
b, c, e = b*2 , 123141, 'ola'
print b
print c 
