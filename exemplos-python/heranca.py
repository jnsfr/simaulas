class SuperClass:

    def superMetodo(self):
        return 'Super HelloWorld'

# sintax da heranca
class MyClass(SuperClass):
    """Documentacao vem aqui"""

    def __init__(self):
    	# construtor (Vazio neste caso)
    	# self equivale ao "this" de java/c
    	self.i = 1111111

    # campo publico
    i = 12345

    # metodos
    def f(self):
        return 'hello world'


x = SuperClass()

print (x.superMetodo())

#erro ! nao acessivel
#print (x.f())

y = MyClass()
### acessivel !
print(y.f())
print(y.superMetodo())

#podemos adicionar variaveis !
#cuidado com acesso atraves do nome errado
y.nome = 'bla'
y.id = 1
print (y.nome)
print (y.id)
