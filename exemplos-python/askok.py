def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        # le input
        ok = raw_input(prompt)
        # o que escrevemos esta nestas hipoteses?
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            #causa um erro !
            raise ValueError('invalid user response')
        # caso nao entre em nenhum lado faz print ao reminder
        print(reminder)

ask_ok('sair?')
ask_ok('serio? uma oportunidade!', 1)
ask_ok('quer mesmo?',1 , 'Tenta de novo..')
