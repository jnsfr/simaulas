
#abrir um ficheiro e colocar conteudos
# outros modos estilo unix (r , a , w)
fp = open('hello.txt', 'w') 
fp.write('The quick brown fox\n')
fp.write('jumps over the lazy dog\n')
fp.writelines(["The quick brown fox\n", "jumps over the lazy dog\n"])
fp.flush() # para esvaziar o buffer de escrita para ficheiro 
fp.close() # para fechar e terminar a escrita
print('--')
fp = open('hello.txt', 'r') 
print fp.read() # le de uma vez
fp.close()
print('--')
# outra maneira que faz com que seja desnecessario .close()
with open('hello.txt', 'r') as fp:
	# ler uma linha 
	linha = fp.readline() 
	print (linha)

print('--')
with open('hello.txt', 'r') as fp:
	# ler linhas todas para uma lista
	linhas = fp.readlines()
	print(linhas)
	for l in linhas:
		print (l)
