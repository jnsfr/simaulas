# README #
Repositório para conteúdos práticos das aulas de Sistemas de Informação Multimédia 2017/2018.

João Nuno Correia, email: 
joaonunocorreia@ulusofona.pt

O repositório será actualizado regularmente com mais conteúdos e correções. Verifiquem sempre se têm a última versão dos ficheiros do repositório.

#exemplos-php:
Contém os exemplos dados nos slides das práticas.


#Miniprojeto - 1 (enunciado no moodle):

Devem analisar os ficheiros da pasta miniprojeto-1. Seguindo e resolvendo correctamente os passos a COMPLETAR e DESCOMENTAR em comentário no código fornecido deve ser suficiente para a resolução do projeto. Outras abordagens que resolvam o problema serão aceites. O extra do enunciado não tem passos e destina-se a quem já resolveu o projeto e quer aprofundar conhecimentos.


Passos:

1 - aceder ao phpmyadmin 
1.1 - criar uma base de dados com o nome 'sim' (isto para não terem de alterar depois em auxiliar.php)

1.2 - selecionando no menu à esquerda a base de dados 'sim' selecionar o separador SQL

1.3 - inserir o código de miniprojeto-1/galery.sql e executar (botão à direita). Com este passo a tabela galery será criada e deverá aparecer à esquerda dentro da base de dados 'sim'.


2 - copiar os ficheiros .php da pasta miniprojeto-1 para uma pasta do servidor apache (wamp: /wwww/ ; mamp,xampp,etc... : /htdocs/ ), para ficar mais organizado criem uma nova pasta por exemplo:   c:/wamp/wwww/sim/
(A partir daqui dentro dos ficheiros .php existem comentários a explicar e a assinalar as partes a completar)
2.1 - o primeiro ficheiro a analisar será 'upload.php' . Será para completar o formulário com um campo de texto e passar para o próximo ficheiro a analisar: 'gravar.php'

2.2 - o 'gravar.php' precisa de chamar o 'auxiliar.php' e ser completado na sua totalidade para avançar. Se tudo correr bem são encaminhados para a próxima página: 'galeria.php'. Podem usar a página php-admin para consultar o conteúdo da base de dados. Caso tenha executado correctamente deverá ter sido adicionada uma nova entrada na base de dados. Nota: para debug podem comentar a linha "header("location:galeria.php");", impedindo que seja reencaminhada a página para a galeria.php .

2.3 - a 'galeria.php' deverá mostrar o conteudo da base de dados: nome e imagem. Devem recorrer à função por completar em 'auxiliar.php'

2.4 - a 'auxiliar.php' tem um conjunto de funções para lidar com a base de dados. As implementadas serão para ser analisadas e usadas convenientemente para resolver os pontos acima. A função por completar entra no 2.3. A ideia será a função construir a parte de html da página para apresentar o conteudo da base de dados.





Notas:

1 - Para poderem efectuar o upload de imagens devem verificar o ficheiro php.ini procurar pela secção File Uploads (linha 805 no wamp).

2 - Utilizem ficheiros jpeg ou jpg com menos de 2 megas ou até mesmo Kbs por causa do limite do campo da base de dados.

3 - verifiquem limites da cache mysql em 'my.cnf' do SQL caso estejam a enviar ficheiros com mais de 500Kb.


Material extra:

http://www.mysqltutorial.org/php-mysql-blob/
http://www.codingcage.com/2016/02/upload-insert-update-delete-image-using.html
http://www.corriejgreen.co.uk/blog/posts/php-mysql-pdo-upload-image-to-server-12/

video:
https://www.youtube.com/watch?v=dphrZUrfeis

Mini projeto 2 - Text Analysis Python
(enunciado e detalhes no moodle)

Mini projeto 3 - Sound Analysis Python 
(baseado no conteudo do moodle, ver enunciado)

Devem completar o ipynb fornecido. Servirá como entrega de código e relatório. Deve ser feita a análise do código na integra antes de começar a implementação pois certas partes estão implementadas. Procurar (COMPLETAR/IMPLEMENTAR) e editar de acordo os comentários e passos fornecidos.

A pipeline para a realização deste projeto será a seguinte:
1. Ler um conjunto de ficheiros áudio em ficheiro, incluídos no projeto (pasta: ”dataset”);
2. Pre-processamento do áudio, normalizando o tamanho a analisar pela faixa de som com um tempo fixo de duração.
3. Aplicação da Short Term Fourier Transform (STFT) sobre os diferentes sinais áudio para passar de uma série temporal para amplitude/frequência.
4. Concatenar o resultado da STFT numa matriz de amplitude/frequência única por espécie V.
5. Aplicação do NMF a cada matriz V para extração de N componentes, obtendo as matrizes W e H.
6. Concatenar as colunas das matrizes W de cada espécie numa só matriz T, obtendo assim os Spectral Basis Vectors (SBVs) de todas as espécies.
7. Aplicar NMF sobre a matriz T obtida no ponto 5 e analisar os coeficientes obtidos. 
8. Produzir relatórios textuais e gráficos para descrever em detalhe o conhecimento adquirido após a aplicação dupla do NMF a este dataset



Notas:

1. para os users em windows devem instalar o conjunto de codecs audio à parte. 
( https://librosa.github.io/librosa/install.html )
FFMPEG:
https://ffmpeg.zeranoe.com/builds/
1. Fazer o download da build do ffmpeg em zip.
2. Adicionar à PATH do sistema a pasta bin, algo neste sentido:
ffmpeg-<versao>-win64-static\bin





