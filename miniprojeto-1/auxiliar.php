<?php
// cria a ligação necessária para uma base de dados com o nome 'sim' (deve ser criada através do php admin)
// devolve: ligação à base de dados
function create_database_connection(){

	try {
		// args: <ligação em url> , <user_bd> , <pass_bd>
	    $pdo = new PDO('mysql:host=localhost;dbname=sim', 'root', 'mysql');
	    return $pdo;
	} catch (PDOException $e) {
	    print $e->getMessage();
	    return false;
	}

}

// destroi a referência
function destroy_database_connection(&$pdo){
	$pdo = null;
}

// executa uma query para ir buscar todos os campos da tabela galery
// argumentos : $pdo - ligação à base de dados 
// devolve : $all_images - array com elementos da tabela, caso existam elementos na tabela. 
// podemos aceder aos campos como se fosse um dicionário: $all_images[elemento_i]['nome'], $all_images[elemento_i]['imagem']
function get_all_images($pdo){
	$statement = $pdo->prepare("SELECT * FROM galery");
	$statement->execute();
	$all_images = $statement->fetchAll();
	return $all_images;
}

// COMPLETAR escreve a página da galeria. Nesta versão simples, por cada registo da base de dados, escreve o nome numa linha e mostra a imagem por baixo.
// argumentos: $pdo - ligação à base de dados
function show_all_images($pdo){
	
	// 1 . através da ligação '$pdo' ir buscar todos os registos da tabela 'galery'

	// 2. iterar sobre os registos (elementos do array)

	// 2.1 "escrever" a página mostrando o nome e a imagem
		$nome; // COMPLETAR - referência para o campo da base de dados 'nome'
		echo $nome .'<br>';// DESCOMENTAR DEPOIS DE COMPLETAR
		$img; // COMPLETAR - referência para o campo da base de dados 'imagem'
		// DESCOMENTAR DEPOIS DE COMPLETAR
		echo '<img src="data:image;base64,'. base64_encode($img) . '"> <br>';
}

?>

